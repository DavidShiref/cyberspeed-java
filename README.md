# CyberSpeed Chat Room
---

## Description
---

This project is a Java Spring Boot 3 (WebFlux Reactive) application that provides APIs for various messages and includes WebSocket connections for real-time chat.

## Prerequisites
---

Before running the application, ensure you have the following installed:

- Java Development Kit (JDK) 17 or later
- Apache Maven (for building the project)
- PostgreSQL (as the database)

## Installation Steps
---

1. **Clone the Repository**
   ```bash
   git clone https://github.com/your_username/your_project.git

2. **Configure the database:**

Create a new PostgreSQL database.

Update the database configuration properties in application.properties file located in the project's */src/main/resources* directory. Update the following properties:
   ```bash
spring.datasource.url=jdbc:postgresql://<database-host>:<port>/<database-name>
spring.datasource.username=<database-username>
spring.datasource.password=<database-password>
```
You man need to run database Migration first here the table script 
```aidl
-- DROP TABLE public.message;

CREATE TABLE public.message (
	id int4 DEFAULT nextval('newtable_id_seq'::regclass) NOT NULL,
	"content" varchar NULL,
	created_by_user varchar NULL,
	"type" varchar NULL,
	created_at timestamp NULL,
	deleted_at timestamp NULL,
	CONSTRAINT newtable_pk PRIMARY KEY (id)
);
```

## Usage 
- Access the APIs: Use tools like cURL or Postman to interact with the RESTful APIs provided by the application, You will find Postman Collection for REST APIs **inside Resource folder**.
- WebSocket Chat: Open a WebSocket client and connect to the chat endpoint for real-time communication, for some restriction on postman can't share WebSocket collection but here as example.
  * open new WebSocket Session and use host **ws://localhost:8080/ws** and header Basic Authorization for *username*=**USER**, *password*=**USER** and connect for session 1 and start sending message like ***{"content":"Hi from user 1"}***. 
  * Repeate this steps for session 2 and 3 as you will be able to see the latest message on new connections
  * check your database as you will see messages start coming.
  * use delete API for deleting and not shown for new users who are joining the room.

## TODO/Enhancement
- Docker file
- Send internal(silent) messages type for online users to update UIs
- CI/CD
