package com.cyberspeed.presistance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.annotation.Nullable;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;


@Table("message")
public record Message(
        @Id
        @Nullable
        Integer id,
        String content,
        @Nullable
        String createdByUser,
        @Nullable
        Type type,
        @Nullable
        Instant deletedAt
) {

    public enum Type {
        CHAT_MESSAGE, USER_LEFT, USER_JOIN;
    }

    public Message {
        if (type == null) {
            type = Type.CHAT_MESSAGE;
        }
    }

    public Message deleteMessage() {
        return new Message(id, content, createdByUser,type, Instant.now());
    }

}
