package com.cyberspeed.presistance.repository;

import com.cyberspeed.presistance.model.Message;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface MessageRepository extends ReactiveCrudRepository<Message, Integer> {

    Flux<Message> findByIdGreaterThanAndDeletedAtIsNull(Integer id);

    Mono<Message> findByIdAndDeletedAtIsNull(Integer id);
}
