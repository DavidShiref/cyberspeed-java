package com.cyberspeed.controller;

import com.cyberspeed.presistance.model.Message;
import com.cyberspeed.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController()
@RequestMapping("messages/")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping(path = "/", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Message> getMessages(@RequestParam(value = "nextCursor", required = false) Integer nextCursor) {
        return messageService.getMessages(Mono.justOrEmpty(nextCursor));
    }

    @DeleteMapping(path = "/{id}")
    public Mono<Boolean> delete(@PathVariable(value = "id", required = true) Integer id) {
        return messageService.softDelete(id);
    }

}
