package com.cyberspeed.service;

import com.cyberspeed.presistance.model.Message;
import com.cyberspeed.presistance.repository.MessageRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import org.springframework.web.reactive.socket.WebSocketMessage;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.util.Optional;

import static com.cyberspeed.presistance.model.Message.Type.USER_JOIN;
import static com.cyberspeed.presistance.model.Message.Type.USER_LEFT;


public class ChatRoomHandler implements WebSocketHandler {

    private MessageRepository messageRepository;
    private Sinks.Many<Message> messagePublisher;
    private Flux<String> outputEvents;
    private ObjectMapper mapper;

    public ChatRoomHandler(Sinks.Many<Message> messagePublisher, Flux<Message> events, MessageRepository messageRepository) {
        this.messagePublisher = messagePublisher;
        this.mapper = new ObjectMapper();
        this.outputEvents = Flux.from(
                events.flatMap(msg -> messageRepository.findByIdAndDeletedAtIsNull(msg.id())
                        .map(v -> true)
                        .defaultIfEmpty(false)
                        .flatMap(found -> found ? Mono.just(msg) : Mono.empty()))
        ).map(this::toJSON);
        this.messageRepository = messageRepository;
    }

    @Override
    public Mono<Void> handle(WebSocketSession session) {
        WebSocketMessageSubscriber subscriber = new WebSocketMessageSubscriber(messagePublisher);
        session.getHandshakeInfo()
                .getPrincipal()
                .flatMap(principal -> messageRepository.save(new Message(null, null, principal.getName(), USER_JOIN, null)))
                .subscribe();
        return session.receive()
                .map(WebSocketMessage::getPayloadAsText)
                .map(this::toEvent)
                .doOnNext(subscriber::onNext)
                .doOnError(subscriber::onError)
                .doOnComplete(subscriber::onComplete)
                .zipWith(session.send(outputEvents.map(session::textMessage)))
                .then();
    }

    private Message toEvent(String json) {
        try {
            return mapper.readValue(json, Message.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String toJSON(Message event) {
        try {
            return mapper.writeValueAsString(event);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    class WebSocketMessageSubscriber {
        private Sinks.Many<Message> eventPublisher;
        private Optional<Message> lastReceivedEvent = Optional.empty();

        public WebSocketMessageSubscriber(Sinks.Many<Message> eventPublisher) {
            this.eventPublisher = eventPublisher;
        }

        public void onNext(Message event) {
            lastReceivedEvent = Optional.of(event);
            messageRepository.save(event)
                    .doOnNext(eventPublisher::tryEmitNext)
                    .subscribe();

        }

        public void onError(Throwable error) {
            //TODO log/format error
            error.printStackTrace();
        }

        public void onComplete() {
            lastReceivedEvent.ifPresent(
                    event ->
                            messageRepository.save(new Message(null, null, event.createdByUser(), USER_LEFT, null))
                                    .map(saved ->
                                            eventPublisher.tryEmitNext(saved)
                                    )
            );

        }

    }
}
