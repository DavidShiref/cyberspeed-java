package com.cyberspeed.service;

import com.cyberspeed.presistance.model.Message;
import com.cyberspeed.presistance.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    public Flux<Message> getMessages(Mono<Integer> nextCursor) {
        return nextCursor
                .flatMapMany(id -> messageRepository.findByIdGreaterThanAndDeletedAtIsNull(id))
                .switchIfEmpty(messageRepository.findAll());
    }

    public Mono<Boolean> softDelete(Integer id) {
        return messageRepository.findByIdAndDeletedAtIsNull(id)
                .flatMap(msg ->
                        messageRepository.save(msg.deleteMessage())
                                .thenReturn(true)
                ).defaultIfEmpty(false);

    }
}
