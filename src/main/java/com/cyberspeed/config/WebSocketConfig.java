package com.cyberspeed.config;

import com.cyberspeed.presistance.model.Message;
import com.cyberspeed.presistance.repository.MessageRepository;
import com.cyberspeed.service.ChatRoomHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.util.Pair;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.publisher.UnicastProcessor;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@Configuration
public class WebSocketConfig {

    @Autowired
    private MessageRepository messageRepository;

    @Bean
    public Sinks.Many<Message> eventPublisher() {
        return Sinks.many().unicast().onBackpressureBuffer();
    }

    @Bean
    public Flux<Message> events(Sinks.Many<Message> eventPublisher) {
        return eventPublisher.asFlux()
                .replay(25)
                .autoConnect();
    }

    @Bean
    public SimpleUrlHandlerMapping simpleUrlHandlerMapping(Sinks.Many<Message> eventPublisher, Flux<Message> events) {
        Map<String, Object> map = new HashMap<>();
        map.put("/ws", new ChatRoomHandler(eventPublisher, events, messageRepository));
        SimpleUrlHandlerMapping simpleUrlHandlerMapping = new SimpleUrlHandlerMapping();
        simpleUrlHandlerMapping.setUrlMap(map);
        simpleUrlHandlerMapping.setOrder(10);
        return simpleUrlHandlerMapping;
    }

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }
}
