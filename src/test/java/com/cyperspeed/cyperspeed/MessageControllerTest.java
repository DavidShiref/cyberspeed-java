package com.cyperspeed.cyperspeed;

import com.cyberspeed.controller.MessageController;
import com.cyberspeed.presistance.model.Message;
import com.cyberspeed.service.MessageService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.client.ExchangeFilterFunctions;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@WebFluxTest
public class MessageControllerTest {

    @InjectMocks
    private MessageService messageService;

    @MockBean
    private MessageController messageController;


    private WebTestClient webTestClient = WebTestClient.bindToServer()
            .filter(ExchangeFilterFunctions.basicAuthentication("USER", "USER")).build();

    @Test
    public void testGetMessages() {
        // Mock the response from the messageService
        List<Message> messages = Arrays.asList(
                new Message(1, "Hello", "User1", Message.Type.CHAT_MESSAGE, null),
                new Message(2, "Hi", "User2", Message.Type.CHAT_MESSAGE, null)
        );
        given(messageService.getMessages(any())).willReturn(Flux.fromIterable(messages));
        webTestClient.get()
                .uri("/messages/")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Message.class)
                .isEqualTo(messages);
    }

    @Test
    public void testDelete() {
        Integer messageId = 1;
        given(messageService.softDelete(messageId)).willReturn(Mono.just(true));
        webTestClient.delete().uri("/messages/{id}", messageId)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Boolean.class)
                .isEqualTo(true);
    }
}
